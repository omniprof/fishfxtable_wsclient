package com.kenfogel.fishfxtable.view;

import com.kenfogel.fishfxtable.beans.FishData;
import com.kenfogel.fishfxtable.ws.Fish;
import com.kenfogel.fishfxtable.ws.FishJPAWebService;
import com.kenfogel.fishfxtable.ws.FishService;
import java.sql.SQLException;
import java.util.List;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FishFXTableController {

    private final Logger log = LoggerFactory.getLogger(this.getClass()
            .getName());

    @FXML
    private AnchorPane fishFXTable;

    @FXML
    private TableView<FishData> fishDataTable;

    @FXML
    private TableColumn<FishData, Number> idColumn;

    @FXML
    private TableColumn<FishData, String> commonNameColumn;

    @FXML
    private TableColumn<FishData, String> latinColumn;

    @FXML
    private TableColumn<FishData, String> phColumn;

    @FXML
    private TableColumn<FishData, String> khColumn;

    @FXML
    private TableColumn<FishData, String> tempColumn;

    @FXML
    private TableColumn<FishData, String> fishSizeColumn;

    @FXML
    private TableColumn<FishData, String> speciesOriginColumn;

    @FXML
    private TableColumn<FishData, String> tankSizeColumn;

    @FXML
    private TableColumn<FishData, String> stockingColumn;

    @FXML
    private TableColumn<FishData, String> dietColumn;

    /**
     * The constructor. The constructor is called before the initialize()
     * method.
     */
    public FishFXTableController() {
        super();
        log.info("FishFXTableController instantiated");
    }

    /**
     * Initializes the controller class. This method is automatically called
     * after the fxml file has been loaded.
     */
    @FXML
    private void initialize() {

        // Connects the property in the FishData object to the column in the table
        idColumn.setCellValueFactory(cellData -> cellData.getValue()
                .idProperty());
        commonNameColumn.setCellValueFactory(cellData -> cellData.getValue()
                .commonNameProperty());
        latinColumn.setCellValueFactory(cellData -> cellData.getValue()
                .latinProperty());
        phColumn.setCellValueFactory(cellData -> cellData.getValue()
                .phProperty());
        khColumn.setCellValueFactory(cellData -> cellData.getValue()
                .khProperty());
        tempColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tempProperty());
        fishSizeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .fishSizeProperty());
        speciesOriginColumn.setCellValueFactory(cellData -> cellData.getValue()
                .speciesOriginProperty());
        tankSizeColumn.setCellValueFactory(cellData -> cellData.getValue()
                .tankSizeProperty());
        stockingColumn.setCellValueFactory(cellData -> cellData.getValue()
                .stockingProperty());
        dietColumn.setCellValueFactory(cellData -> cellData.getValue()
                .dietProperty());
        fishDataTable.autosize();
        adjustColumnWidths();
    }

    /**
     * Retrieve the data from the web service, convert it to aa ObservableList
     * and display it
     *
     * @throws SQLException
     */
    public void displayTheTable() throws SQLException {
        try { // Call Web Service Operation
            FishService service = new FishService();
            FishJPAWebService port = service.getFishJPAWebServicePort();

            // TODO process result here
            List<Fish> result = port.retriveFish();

            // Must convert List of Fish entities to an ObservableList of FishData
            ObservableList<FishData> fishList = FXCollections.observableArrayList();
            result.forEach((f) -> {
                fishList.add(new FishData(f));
            });

            // Show the fish
            fishDataTable.setItems(fishList);
        } catch (Exception ex) {
            log.error("Error retrieving fish", ex);
            errorAlert(ex.getMessage());
        }
    }

    /**
     * Sets the width of the columns based on a percentage of the overall width
     */
    private void adjustColumnWidths() {
        // Get the current width of the table
        double width = fishFXTable.getPrefWidth();
        // Set width of each column
        idColumn.setPrefWidth(width * .05);
        commonNameColumn.setPrefWidth(width * .15);
        latinColumn.setPrefWidth(width * .15);
        phColumn.setPrefWidth(width * .05);
        khColumn.setPrefWidth(width * .05);
        tempColumn.setPrefWidth(width * .05);
        fishSizeColumn.setPrefWidth(width * .1);
        speciesOriginColumn.setPrefWidth(width * .1);
        tankSizeColumn.setPrefWidth(width * .1);
        stockingColumn.setPrefWidth(width * .1);
        dietColumn.setPrefWidth(width * .1);
    }

    /**
     * Error message popup dialog
     *
     * @param msg
     */
    private void errorAlert(String msg) {
        Alert dialog = new Alert(Alert.AlertType.ERROR);
        dialog.setTitle("Table Controller Error");
        dialog.setHeaderText("ERROR");
        dialog.setContentText(msg);
        dialog.show();

    }

}
